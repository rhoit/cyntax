# include <string.h>
# include <stdio.h>

# define paint(x) printf(x)
/******************************************
	BLACK=30 RED=31 GREEN=32 YELLOW=33
	BLUE=34 MAGENTA=35 CYAN=36 WHITE=37
*******************************************/

# define NORMAL "\033[0;30m"
# define PREPROCESS "\033[3;35m"
# define COMMENT "\033[0;34m"
# define QUOTE1 "\033[0;31m"
# define QUOTE2 "\033[1;31m"
# define DIGIT "\033[2;36m"
# define KEYWORD "\033[0;32m"
# define DATATYPE "\033[7;32m"
# define CONDITION "\033[2;31m"
# define LOOP "\033[9;32m"

# define digit ( pbuff>='0' && pbuff<='9' )
# define Alpha ( pbuff>='A' && pbuff<='Z' )
# define alpha ( pbuff>='a' && pbuff<='z' )
# define ALPHA ( Alpha || alpha )
# define ALNUM ( ALPHA || digit )

int flag_LOCK;
int flag_ESCAPE;
int flag_COMMENT;
int flag_PREPROCESSOR;
int flag_QUOTE1;
int flag_QUOTE2;
int flag_DIGIT;
int flag_FUNCTION;
int flag_KEYWORD;

# define ALL_FLAGS_DOWN {\
	flag_LOCK=0;\
	flag_ESCAPE=0;\
	flag_COMMENT=0;\
	flag_PREPROCESSOR=0;\
	flag_QUOTE1=0;\
	flag_QUOTE2=0;\
	flag_DIGIT=0;\
	flag_KEYWORD=0;\
}

int parser();
int wordparser(int);

FILE *fp;
int len;
char word[80];

int main(int argc, char *argv[]) {
	if(argc>1) fp=fopen(argv[1], "r");
	else {
		printf("ERROR: missing argument\n");
		return 2;
    }

	if(!fp) {
		printf("ERROR: %s isn't the plain text file\n", argv[1]);
		return 2;
	}

	ALL_FLAGS_DOWN;
	while(parser()!=EOF); //yes its the overhead i know it
	return 0;
}

int wordparser(int pbuff) {
	len=0;

	flag_KEYWORD=1;
	while(1) { //grab word
		word[len]=pbuff;
		pbuff=fgetc(fp);
		if(pbuff==EOF) return EOF;
		if(!alpha) flag_KEYWORD=0; //function
		len++;
		if(!( ALNUM || pbuff=='_')) break;
	}

	word[len]='\0';

	if(flag_KEYWORD) {
		printf("%s",word);
		return pbuff;
	}

	switch(len) { //reducing comparision
	case 2:
		if(strcmp(word, "do")==0) flag_KEYWORD='l';
		else if(strcmp(word, "if")==0) flag_KEYWORD='c';
		break;

	case 3:
		if(strcmp(word, "int")==0) flag_KEYWORD='d';
		else if(strcmp(word, "for")==0) flag_KEYWORD='l';
		break;

	case 4:
		if(strcmp(word, "auto")==0) flag_KEYWORD='d';
		else if(strcmp(word, "case")==0) flag_KEYWORD='c';
		else if(strcmp(word, "char")==0) flag_KEYWORD='d';
		else if(strcmp(word, "else")==0) flag_KEYWORD='c';
		else if(strcmp(word, "enum")==0) flag_KEYWORD='d';
		else if(strcmp(word, "goto")==0) flag_KEYWORD='k';
		else if(strcmp(word, "long")==0) flag_KEYWORD='d';
		else if(strcmp(word, "void")==0) flag_KEYWORD='d';
		break;

	case 5:
		if(strcmp(word, "break")==0) flag_KEYWORD='c';
		else if(strcmp(word, "const")==0) flag_KEYWORD='k';
		else if(strcmp(word, "float")==0) flag_KEYWORD='d';
		else if(strcmp(word, "short")==0) flag_KEYWORD='d';
		else if(strcmp(word, "union")==0) flag_KEYWORD='d';
		else if(strcmp(word, "while")==0) flag_KEYWORD='l';
		break;

	case 6:
		if(strcmp(word, "double")==0) flag_KEYWORD='d';
		else if(strcmp(word, "extern")==0) flag_KEYWORD='d';
		else if(strcmp(word, "inline")==0) flag_KEYWORD='k';
		else if(strcmp(word, "return")==0) flag_KEYWORD='k';
		else if(strcmp(word, "signed")==0) flag_KEYWORD='d';
		else if(strcmp(word, "sizeof")==0) flag_KEYWORD='k';
		else if(strcmp(word, "static")==0) flag_KEYWORD='d';
		else if(strcmp(word, "struct")==0) flag_KEYWORD='d';
		else if(strcmp(word, "switch")==0) flag_KEYWORD='c';
		break;

	case 7:
		if(strcmp(word, "default")==0) flag_KEYWORD='k';
		else if(strcmp(word, "typedef")==0) flag_KEYWORD='d';
		break;

	case 8:
		if(strcmp(word, "continue")==0) flag_KEYWORD='c';
		else if(strcmp(word, "unsigned")==0) flag_KEYWORD='d';
		else if(strcmp(word, "register")==0) flag_KEYWORD='d';
		else if(strcmp(word, "volatile")==0) flag_KEYWORD='d';
		break;
	}

	switch(flag_KEYWORD) {
	case 'c': paint(CONDITION); break;
	case 'd': paint(DATATYPE); break;
	case 'k': paint(KEYWORD); break;
	case 'l': paint(LOOP); break;
	}

	if(flag_KEYWORD) {
		printf("%s",word);
		paint(NORMAL);
	}
	else printf("%s",word);

	flag_KEYWORD=0;
	return pbuff;
}

int parser() {
	int pbuff=fgetc(fp);
	if(pbuff==EOF) return EOF;

	if(flag_ESCAPE) {
		printf("%c", pbuff);
		flag_ESCAPE=0;
		return pbuff;
	}

	if(!flag_LOCK) {
		if(digit) {
			paint(DIGIT);
			printf("%c",pbuff);
			paint(NORMAL);
			return pbuff;
		}
		if(ALPHA || pbuff=='_') pbuff=wordparser(pbuff);
	}

	switch(pbuff) {
	case '\t':
		printf("%4c", ' ');
		return pbuff;

	case '\\':
		flag_ESCAPE=1;
		break;

	case '\n':
		if(!flag_LOCK) break;
		if (flag_COMMENT==2 || flag_COMMENT==-2) break; //skip multiline comment
		flag_LOCK--;
		flag_PREPROCESSOR=0;
		flag_COMMENT=0;
		paint(NORMAL);
		break;

	case '\'':
		if(flag_QUOTE1) {
			flag_QUOTE1=0;
			flag_LOCK--;
			printf("\'");
			paint(NORMAL);
			return pbuff;
		}
		if(flag_LOCK) break;
		paint(QUOTE1);
		flag_QUOTE1=1;
		flag_LOCK++;
		break;

	case '"':
		if(flag_QUOTE2) {
			flag_QUOTE2=0;
			flag_LOCK--;
			printf("\"");
			paint(NORMAL);
			return pbuff;
		}
		if(flag_LOCK) break;
		paint(QUOTE2);
		flag_QUOTE2=1;
		flag_LOCK++;
		break;

	case '*':
		if (flag_COMMENT==2) flag_COMMENT=-2; //endsignal multiline comment
		else if(flag_COMMENT==-1) { //multiline comment begin
			flag_COMMENT=2;
			flag_LOCK++;
			paint(COMMENT);
			printf("/");
		}
		break;

	case '/':
		if(flag_COMMENT==-2) { //multiline comment end
			flag_COMMENT=0;
			flag_LOCK--;
			printf("/");
			paint(NORMAL);
			return pbuff;
		}
		else if (flag_COMMENT==-1) { //singleline comment start
			flag_COMMENT=1;
			flag_LOCK++;
			paint(COMMENT);
			printf("/");
		}
		else { //signal singleline comment start
			if(flag_LOCK) break; //check lock in start signal only :)
			flag_COMMENT=-1;
			return pbuff;
		}
		break;

	case '#':
		if(flag_LOCK) break;
		flag_LOCK++;
		paint(PREPROCESS);
		flag_PREPROCESSOR=1;
		break;

	default:
		if(flag_COMMENT==-2) {
			flag_COMMENT=2;
		}
		else if(flag_COMMENT==-1 ) { //comment false alarm
			printf("/");
			flag_COMMENT=0;
		}
}

	printf("%c", pbuff);
	return pbuff;
}

